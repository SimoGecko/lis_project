import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.preprocessing import StandardScaler,PolynomialFeatures
import time

train = pd.read_csv("train.csv")
test = pd.read_csv("test.csv")

sol = pd.read_csv("sample.csv")

print("Start")
def svmreg(X,y,title):
    global T
    global clf,rs
    
    #split data into train and test sets
#    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=rs)

#    X_train, y_train = shuffle(X_train, y_train, random_state=10)

    X_train = X
    y_train = y
    X_test = T

    print("Train on", len(X_train), "samples. Test on", len(X_test), "samples.")
    #normalize, scale data before training
    sc = StandardScaler()
    sc.fit(X_train)
    X_train = sc.transform(X_train)
    X_test = sc.transform(X_test)

    #fit and predict
    clf.fit(X_train, y_train)
    y_test_pred = clf.predict(X_test)

    return y_test_pred


X = train.loc[:,"x1":"x15"]
T = test.loc[:,"x1":"x15"]
X = X.values
T = T.values
y = train.loc[:,"y"].values


start = time.time()

poly = PolynomialFeatures(degree=3)
X = poly.fit_transform(X)
T = poly.fit_transform(T)

clf = SVC(C=0.7429971445684741,kernel='linear')
my_y_pred = svmreg(X,y,"SVC")

end = time.time()


sol["y"] = my_y_pred
sol.to_csv("mysol.csv",index=False)


print("done")
print(end - start, "seconds")
 