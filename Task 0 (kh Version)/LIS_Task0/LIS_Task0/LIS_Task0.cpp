// LIS_Task0.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdio.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <iomanip>

#include <Eigen/Dense>

using namespace Eigen;
using namespace std;



class dataPoint {
public:
	int id;
	double y;
	double x[10];

	double x_sum;
	double x_mean;

	void calc() {
		x_sum = 0;
		for (int i = 0; i < 10; i++) {
			x_sum += x[i];
		}

		x_mean = x_sum / 10;
	}
};


dataPoint* trainingDataPoints;
dataPoint* testDataPoints;

int numberOfTrainingDataPoints = 10000;
int numberOfTestDataPoints = 2000;


void readTrainCSV(dataPoint* dataPoints);
void readTestCSV(dataPoint* dataPoints);
VectorXd EigenRegression(dataPoint* dataPoints);
VectorXd EigenRegression2(dataPoint* dataPoints);
VectorXd EigenRegression3(dataPoint* dataPoints);
void CreateSampleCSV(dataPoint *testDataPoints, VectorXd solution, string fileName);



int _tmain(int argc, _TCHAR* argv[])
{
	
	trainingDataPoints = new dataPoint[numberOfTrainingDataPoints];
	testDataPoints = new dataPoint[numberOfTestDataPoints];


	readTrainCSV(trainingDataPoints);




	/*cout << "Some Tests:" << endl;

	for (int i = 0; i < 10; i++) {
		cout << "ID:" << to_string(dataPoints[i].id) << endl;
		cout << "y:" << to_string(dataPoints[i].y) << endl;
		cout << "x0" << to_string(dataPoints[i].x[0]) << endl << endl;
	}*/

	/*for (int i = 0; i < 10; i++) {
		cout << "y: " << to_string(dataPoints[i].y) << endl;
		cout << "mean: " << to_string(dataPoints[i].x_mean) << endl << endl;
	}*/

	cout << "Eigen Regression" << endl;


	VectorXd solution;
	VectorXd solution2;
	VectorXd solution3;

	solution = EigenRegression(trainingDataPoints);
	solution2 = EigenRegression2(trainingDataPoints);
	solution3 = EigenRegression3(trainingDataPoints);

	readTestCSV(testDataPoints);
	CreateSampleCSV(testDataPoints, solution, "sample1");
	CreateSampleCSV(testDataPoints, solution2, "sample2");
	CreateSampleCSV(testDataPoints, solution3, "sample3");

	system("pause");
	return 0;
}

void readTrainCSV(dataPoint* dataPoints) {
	cout << "Started CSV Parsing..." << endl << endl;

	ifstream data("train.csv");

	int i = 0;
	int j = 0;

	bool first = true;

	string line;
	while (getline(data, line))
	{
		// Ignore first row (titles)
		if (first) {
			first = false;
			continue;
		}

		j = 0;

		dataPoint dataPoint;

		stringstream lineStream(line);
		string cell;
		while (getline(lineStream, cell, ','))
		{
			if (j == 0) // ID
				dataPoint.id = stoi(cell);
			else if (j == 1) // y
				dataPoint.y = stod(cell);
			else // some x
				dataPoint.x[j - 2] = stod(cell);

			j++;
		}

		dataPoint.calc();
		dataPoints[i] = dataPoint;

		i++;

		// cout << "Finished Parsing Line " << to_string(i) << endl;
	}

	cout << endl;
	cout << "Finished CSV Parsing!" << endl << endl;
}

void readTestCSV(dataPoint* dataPoints) {
	cout << "Started CSV Parsing..." << endl << endl;

	ifstream data("test.csv");

	int i = 0;
	int j = 0;

	bool first = true;

	string line;
	while (getline(data, line))
	{
		// Ignore first row (titles)
		if (first) {
			first = false;
			continue;
		}

		j = 0;

		dataPoint dataPoint;

		stringstream lineStream(line);
		string cell;
		while (getline(lineStream, cell, ','))
		{
			if (j == 0) // ID
				dataPoint.id = stoi(cell);
			else // some x
				dataPoint.x[j - 1] = stod(cell);

			j++;
		}

		dataPoint.calc();
		dataPoints[i] = dataPoint;

		i++;

		// cout << "Finished Parsing Line " << to_string(i) << endl;
	}

	cout << endl;
	cout << "Finished CSV Parsing!" << endl << endl;
}

string to_string_with_precision(double a_value, const int n = 20)
{
	ostringstream out;
	out << setprecision(n) << a_value;
	return out.str();
}


void CreateSampleCSV(dataPoint *testDataPoints, VectorXd solution, string fileName) {
	VectorXd inputs(10);

	for (int i = 0; i < numberOfTestDataPoints; i++) {
		for (int j = 0; j < 10; j++) {
			inputs(j) = testDataPoints[i].x[j];
		}


		double y = inputs.transpose() * solution;
		testDataPoints[i].y = y;
	}

	cout.precision(20);

	ofstream myfile;
	myfile.open(fileName+".csv");
	myfile << "Id,y\n";

	for (int i = 0; i < numberOfTestDataPoints; i++) {
		myfile << to_string(testDataPoints[i].id) << "," << to_string_with_precision(testDataPoints[i].y) << "\n";
	}

	myfile.close();
}

VectorXd EigenRegression(dataPoint* dataPoints) {
	// EIGEN - First Number = Zeilen ; Second Number = Kolonnen

	int numberOfUsedTests = numberOfTrainingDataPoints;

	MatrixXd A(numberOfUsedTests, 10);
	VectorXd b(numberOfUsedTests);

	for (int i = 0; i < numberOfUsedTests; i++) {
		for (int j = 0; j < 10; j++) {
			A(i, j) = dataPoints[i].x[j];
		}

		b(i) = dataPoints[i].y;
	}

	IOFormat HeavyFmt(FullPrecision);

	VectorXd solution = A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b);
	//VectorXd solution = A.fullPivHouseholderQr().solve(b);
	//VectorXd solution = (A.transpose() * A).ldlt().solve(A.transpose() * b);

	cout << "The least-squares solution is:\n"
		<< (A.jacobiSvd(ComputeThinU | ComputeThinV).solve(b)).format(HeavyFmt) << endl;

	cout << "The solution using the QR decomposition is:\n"
		<< (A.colPivHouseholderQr().solve(b)).format(HeavyFmt) << endl;

	cout << "The solution using normal equations is:\n"
		<< ((A.transpose() * A).ldlt().solve(A.transpose() * b)).format(HeavyFmt) << endl;

	return solution;
}

VectorXd EigenRegression2(dataPoint* dataPoints) {
	// EIGEN - First Number = Zeilen ; Second Number = Kolonnen

	int numberOfUsedTests = numberOfTrainingDataPoints;

	MatrixXd A(numberOfUsedTests, 10);
	VectorXd b(numberOfUsedTests);

	for (int i = 0; i < numberOfUsedTests; i++) {
		for (int j = 0; j < 10; j++) {
			A(i, j) = dataPoints[i].x[j];
		}

		b(i) = dataPoints[i].y;
	}

	IOFormat HeavyFmt(FullPrecision);

	VectorXd solution = A.fullPivHouseholderQr().solve(b);

	return solution;
}

VectorXd EigenRegression3(dataPoint* dataPoints) {
	// EIGEN - First Number = Zeilen ; Second Number = Kolonnen

	int numberOfUsedTests = numberOfTrainingDataPoints;

	MatrixXd A(numberOfUsedTests, 10);
	VectorXd b(numberOfUsedTests);

	for (int i = 0; i < numberOfUsedTests; i++) {
		for (int j = 0; j < 10; j++) {
			A(i, j) = dataPoints[i].x[j];
		}

		b(i) = dataPoints[i].y;
	}

	IOFormat HeavyFmt(FullPrecision);

	VectorXd solution = (A.transpose() * A).ldlt().solve(A.transpose() * b);

	return solution;
}