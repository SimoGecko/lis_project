#pragma once

#define TEST_SIZE 3000
#define TRAINING_SIZE 1000
#define SIZE_VECTOR_X 15
#define VALIDATION_SIZE 100
#define NEW_TRAINING_SIZE (TRAINING_SIZE - VALIDATION_SIZE)
#define NUMBER_OF_TESTS 150

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <Eigen\Dense>
#include <Eigen\Eigenvalues>
#include <vector>
#include <iomanip>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\ml\ml.hpp>

//#include "Util.h"

using namespace std;
using namespace Eigen;
using namespace cv;
