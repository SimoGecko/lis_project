// classification.cpp : Defines the entry point for the console application.
//

/* Useful links
- Install & deploy OpenCV: http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_visual_studio_Opencv/windows_visual_studio_Opencv.html#windows-visual-studio-how-to
- SVM Introduction: http://docs.opencv.org/2.4/doc/tutorials/ml/introduction_to_svm/introduction_to_svm.html
- SVM Documentary: http://docs.opencv.org/2.4/modules/ml/doc/support_vector_machines.html
*/

//#include "stdafx.h"
#include "includeList.h"
#include "IOManager.h"
#include "SVM.h"
#include "Util.h"


int main()
{
	cout << "LIS task1 - regression - group Lisnisike\n\n" << endl;
	cout << "nhuart\t\tnhuart@student.ethz.ch" << endl;
	cout << "gsimone\t\tgsimone@student.ethz.ch" << endl;
	cout << "hubertyk\tkevin.huberty@juniors.ethz.ch" << endl << endl << endl;

	cout << setprecision(17);

	DataEntry* myTrainingData = new DataEntry[TRAINING_SIZE];
	DataEntry* myTestData = new DataEntry[TEST_SIZE];

	cout << "Reading trainig data ...";
	IOManager myreader;
	myreader.readData("../../data/train0.csv", myTrainingData, true);
	cout << " done!" << endl;

	cout << "Reading test data ...";
	//IOManager myreader;
	myreader.readData("../../data/test0.csv", myTestData, false);
	cout << " done!" << endl;

	Eigen::MatrixXd matrixTrainingData(TRAINING_SIZE, SIZE_VECTOR_X);
	Eigen::VectorXd resultValues(TRAINING_SIZE);


	// matrix building
	cout << "building training matrix X ...";
	for (int i = 0; i < TRAINING_SIZE; ++i) {
		for (int j = 0; j<SIZE_VECTOR_X; ++j)
			matrixTrainingData(i, j) = myTrainingData[i].x[j];
		resultValues(i) = myTrainingData[i].y;
	}
	cout << "done!" << endl;

	Eigen::MatrixXd matrixTestData(TEST_SIZE, SIZE_VECTOR_X);

	cout << "building test matrix X ...";
	for (int i = 0; i < TEST_SIZE; ++i) {
		for (int j = 0; j < SIZE_VECTOR_X; ++j) {
			matrixTestData(i, j) = myTestData[i].x[j];
		}
	}
	cout << "done!" << endl;

	
	//////////////// Support Vector Machine for classification /////////////////
	///
	/// Initalization of data ///
	///
	// Set training data
	float trainingData[NEW_TRAINING_SIZE][SIZE_VECTOR_X];
	float yValuesTraining[NEW_TRAINING_SIZE];

	cout << "building new training matrix as the 800 first rows of the original training matrix X ...";
	for (int i = 0; i < NEW_TRAINING_SIZE; i++) {
		yValuesTraining[i] = (float)myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			trainingData[i][j] = (float)myTrainingData[i].x[j];
		}
	}
	
	Mat trainingDataMat(NEW_TRAINING_SIZE, SIZE_VECTOR_X, CV_32FC1, trainingData);
	Mat resultMat(NEW_TRAINING_SIZE, 1, CV_32FC1, yValuesTraining);
	cout << "done!" << endl;

	// Set validation data
	float validationData[VALIDATION_SIZE][SIZE_VECTOR_X];
	float yValuesValidation[VALIDATION_SIZE];

	cout << "building new test matrix (validation matrix) as the 200 last rows of the original training matrix X ...";
	for (int i = NEW_TRAINING_SIZE; i < TRAINING_SIZE; i++) {
		yValuesValidation[i - NEW_TRAINING_SIZE] = (float)myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			validationData[i - NEW_TRAINING_SIZE][j] = (float)myTrainingData[i].x[j];
		}
	}

	Mat validationDataMat(VALIDATION_SIZE, SIZE_VECTOR_X, CV_32FC1, validationData);
	cout << "done!" << endl;
	// Finished initialization

	// best parameters on the validation set
	CvSVMParams params = bestParameter(trainingDataMat, resultMat, validationDataMat, yValuesValidation);

	//  Make final result
	// Set test data
	float trainingDataFinal[TRAINING_SIZE][SIZE_VECTOR_X];
	float yValuesTrainingFinal[TRAINING_SIZE];

	cout << "Build final training set ...";
	for (int i = 0; i < TRAINING_SIZE; i++) {
		yValuesTrainingFinal[i] = myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			trainingDataFinal[i][j] = myTrainingData[i].x[j];
		}
	}

	Mat trainingDataMatFinal(TRAINING_SIZE, SIZE_VECTOR_X, CV_32FC1, trainingDataFinal);
	Mat resultMatFinal(TRAINING_SIZE, 1, CV_32FC1, yValuesTrainingFinal);
	cout << "done!" << endl;
	
	CvSVM SVM;
	cout << "Train on final training set ...";
	SVM.train(trainingDataMatFinal, resultMatFinal, Mat(), Mat(), params);
	cout << "done!" << endl;

	double testData[TEST_SIZE][SIZE_VECTOR_X];
	cout << "Build final test set ...";
	for (int i = 0; i < TEST_SIZE; i++) {
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			testData[i][j] = myTestData[i].x[j];
		}
	}
	cout << "done!" << endl;

	Mat testDataMat(TEST_SIZE, SIZE_VECTOR_X, CV_32FC1, testData);
	VectorXd testResults(TEST_SIZE);

	cout << "predict final results ...";
	for (int i = 0; i < testDataMat.rows; i++) {
		cv::Mat sample = testDataMat.row(i);
		double result = SVM.predict(sample, false);
		testResults(i) = result;
	}
	cout << "done!" << endl;
	//----------------------------------------------
	 

	// write back

	string filename;
	cout << "Specify filename:" << endl;
	cin >> filename;

	cout << "wrinting results y to file results.csv ...";
	string pathName = "../../results/" + filename + ".csv";
	myreader.writeTestResult(pathName, testResults);
	cout << "done!" << endl;

	system("pause");
    return 0;
}

