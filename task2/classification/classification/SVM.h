#pragma once
#include "includeList.h"


/* Useful links
- Install & deploy OpenCV: http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_visual_studio_Opencv/windows_visual_studio_Opencv.html#windows-visual-studio-how-to
- SVM Introduction: http://docs.opencv.org/2.4/doc/tutorials/ml/introduction_to_svm/introduction_to_svm.html
- SVM Documentary: http://docs.opencv.org/2.4/modules/ml/doc/support_vector_machines.html
*/


// Helper functions
float CalculateAccuracy(float* predictedResults, float* actualResults, int n) {
	float RMSE = 0.0;
	float sum = 0.0;

	for (int i = 0; i < n; i++) {
		sum += (predictedResults[i] == actualResults[i]);
	}

	RMSE = sum / n;

	return RMSE;
}


/*
svm_type = 0 -> C_SVC
svm_type = 1 -> NU_SVC

kernel_type = 0 -> LINEAR
kernel_type = 1 -> POLY
kernel_type = 2 -> RBF
kernel_type = 3 -> SIGMOID
*/
CvSVMParams GetParams(int svm_type, int kernel_type, float degree, float gamma, float coef0, float C, float nu, double p) {
	CvSVMParams params;

	// SVM_TYPE
	if (svm_type == 0)
		params.svm_type = CvSVM::C_SVC;
	else
		params.svm_type = CvSVM::NU_SVC;

	// KERNEL_TYPE
	if (kernel_type == 0)
		params.kernel_type = CvSVM::LINEAR;
	else if (kernel_type == 1)
		params.kernel_type = CvSVM::POLY;
	else if (kernel_type == 2)
		params.kernel_type = CvSVM::RBF;
	else
		params.kernel_type = CvSVM::SIGMOID;

	// REST
	params.C = C; // for CV_SVM_C_SVC, CV_SVM_EPS_SVR and CV_SVM_NU_SVR
	params.p = p; // for CV_SVM_EPS_SVR
	params.degree = degree; // for poly
	params.gamma = gamma; // for poly/rbf/sigmoid
	params.coef0 = coef0; // for poly/sigmoid
	params.nu = nu; // for CV_SVM_NU_SVC, CV_SVM_ONE_CLASS, and CV_SVM_NU_SVR

	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 10000, 1e-12);

	return params;
}

CvSVMParams bestParameter(Mat trainingDataMat, Mat resultMat, Mat validationDataMat, float* yValuesValidation) {

	float Accuracy[NUMBER_OF_TESTS];
	float biggest_accuracy = 0;

	int bestParam_svmType = 0;
	int bestParam_kernelType = 1;
	float bestParam_C = 1;
	float bestParam_p = 0.1;
	float bestParam_degree = 1.0;
	float bestParam_gamma = 0.1;
	float bestParam_coef0 = 0.1;
	float bestParam_nu = 0.1;

	int currentParam_svmType = 0;
	int currentParam_kernelType = 1;
	float currentParam_C = 1;
	float currentParam_p = 0.1;
	float currentParam_degree = 3.5;
	float currentParam_gamma = 0.1;
	float currentParam_coef0 = 0.1;
	float currentParam_nu = 0.1;

	const float paramMax_C = 150;
	const float paramMax_p = 10;
	const float paramMax_degree = 10;
	const float paramMax_gamma = 10;
	const float paramMax_coef0 = 5;
	const float paramMax_nu = 10;

	const float paramMin_C = 4;
	const float paramMin_p = 0.1;
	const float paramMin_degree = 1;
	const float paramMin_gamma = 0.1;
	const float paramMin_coef0 = 0.1;
	const float paramMin_nu = 0.1;

	// Binary classification with parameter
	for (int n = 0; n < NUMBER_OF_TESTS; n++) {
		// Update params
		currentParam_C = paramMin_C + (n * (paramMax_C / NUMBER_OF_TESTS));
		currentParam_p = paramMin_p + (n * (paramMax_p / NUMBER_OF_TESTS));
		currentParam_nu = paramMin_nu + (n * (paramMax_nu / NUMBER_OF_TESTS));
		currentParam_gamma = paramMin_gamma + (n * (paramMax_gamma / NUMBER_OF_TESTS));

		/*
		if (n % 2 == 0) {
		currentParam_svmType = 0;
		currentParam_kernelType = 1;
		}
		else {
		currentParam_svmType = 1;
		currentParam_kernelType = 2;
		}
		*/

		// Get params & train
		CvSVMParams params = GetParams(currentParam_svmType, currentParam_kernelType, currentParam_degree, currentParam_gamma, currentParam_coef0, currentParam_C, currentParam_nu, currentParam_p);

		CvSVM SVM;
		SVM.train(trainingDataMat, resultMat, Mat(), Mat(), params);
		//SVM.train_auto(trainingDataMat, resultMat, Mat(), Mat(), params);

		// Validate & calculate error
		float validationResults[VALIDATION_SIZE];

		for (int i = 0; i < VALIDATION_SIZE; i++) {
			cv::Mat sample = validationDataMat.row(i);
			float result = SVM.predict(sample, false);
			// cout << result << endl;
			validationResults[i] = (float)result;
		}

		float accuracy = CalculateAccuracy(validationResults, yValuesValidation, VALIDATION_SIZE);
		Accuracy[n] = accuracy;

		if (accuracy > biggest_accuracy) {
			cout << "Accuracy test #" << n << " = " << accuracy << endl;
			biggest_accuracy = accuracy;

			bestParam_C = currentParam_C;
			bestParam_p = currentParam_p;
			bestParam_degree = currentParam_degree;
			bestParam_gamma = currentParam_gamma;
			bestParam_coef0 = currentParam_coef0;
			bestParam_nu = currentParam_nu;
			bestParam_svmType = currentParam_svmType;
			bestParam_kernelType = currentParam_kernelType;

			//cout << "C: " << bestParam_C << endl << "p: " << bestParam_p << endl << "degree: " << bestParam_degree << endl;
			//cout << "gamma: " << bestParam_gamma << endl;
			//cout << "coef0: " << bestParam_coef0 << endl << "nu: " << bestParam_nu << endl << "svmtype: " << bestParam_svmType;
			//cout << endl << "kernel: " << bestParam_kernelType << endl << endl;
		}

		 //cout << "Accuracy test #" << n << " = " << accuracy << endl;
	}

	cout << endl << "===============================" << endl;
	cout << "biggest accuracy: " << biggest_accuracy << endl;

	cout << "C: " << bestParam_C << endl << "p: " << bestParam_p << endl << "degree: " << bestParam_degree << endl;
	cout << "gamma: " << bestParam_gamma << endl;
	cout << "coef0: " << bestParam_coef0 << endl << "nu: " << bestParam_nu << endl << "svmtype: " << bestParam_svmType;
	cout << endl << "kernel: " << bestParam_kernelType << endl << endl;
	CvSVMParams params = GetParams(bestParam_svmType, bestParam_kernelType, bestParam_degree, bestParam_gamma, bestParam_coef0, bestParam_C, bestParam_nu, bestParam_p);

	return params;
}