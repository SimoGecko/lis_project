// this is a class that does operation on matrices, like copying data

#include "includeList.h"

class MatrixOp {
public:
	MatrixOp();
	~MatrixOp();

	void MakePoly2MatrixFromX(MatrixXd* X, MatrixXd* Xpoly2);
	void MakePolyN(MatrixXd* X, MatrixXd* Xpoly, int deg);

	int MatrixOp::ComputePolyDimension(int x_size, int deg);

private:
};