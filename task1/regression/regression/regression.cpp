#include "includeList.h"

#include "IOManager.h"
#include "MatrixOp.h"
#include "Util.h"
#include "SVM.h"



#define TEST_SIZE 2000
#define TRAINING_SIZE 900
#define SIZE_VECTOR_X 15
#define SIZE_VECTOR_X_POLY2 136

<<<<<<< HEAD
using namespace cv;

=======
>>>>>>> 49f410d96fc6810421065817e0ba1dc52b191557
// for PCA
struct eigenStruct {
	double lambda;
	VectorXd vector;
<<<<<<< HEAD
=======

	eigenStruct(double eigenValue, VectorXd eigenVector) : lambda(eigenValue), vector(eigenVector)
	{
	}
};

bool compare(eigenStruct &a, eigenStruct &b) { return a.lambda >= b.lambda; }

>>>>>>> 49f410d96fc6810421065817e0ba1dc52b191557

	eigenStruct(double eigenValue, VectorXd eigenVector) : lambda(eigenValue), vector(eigenVector)
	{
	}
};

bool compare(eigenStruct &a, eigenStruct &b) { return a.lambda >= b.lambda; }



int main()
{
	cout << "LIS task1 - regression - group Lisnisike\n\n" << endl;
	cout << "nhuart\t\tnhuart@student.ethz.ch" << endl;
	cout << "gsimone\t\tgsimone@student.ethz.ch" << endl;
	cout << "hubertyk\tkevin.huberty@juniors.ethz.ch" << endl << endl << endl;

	cout << setprecision(17);

	DataEntry* myTrainingData = new DataEntry[TRAINING_SIZE];
	DataEntry* myTestData = new DataEntry[TEST_SIZE];

	cout << "Reading trainig data ...";
	IOManager myreader;
	myreader.readData("../../data/train0.csv", myTrainingData, true);
	cout << " done!" << endl;

	cout << "Reading test data ...";
	//IOManager myreader;
	myreader.readData("../../data/test0.csv", myTestData, false);
	cout << " done!" << endl;

	/*
	while (true)
	{
	cout << "Insert # to retrieve data:" << endl;
	int i;
	cin >> i;
	if (i < 0 || i >= TEST_SIZE) continue;
	myTrainingData[i].print();
	}
	*/




	Eigen::MatrixXd matrixTrainingData(TRAINING_SIZE, SIZE_VECTOR_X);
	Eigen::VectorXd resultValues(TRAINING_SIZE);


	// matrix building
	cout << "building training matrix X ...";
	for (int i = 0; i < TRAINING_SIZE; ++i) {
		for (int j = 0; j<SIZE_VECTOR_X; ++j)
			matrixTrainingData(i, j) = myTrainingData[i].x[j];
		resultValues(i) = myTrainingData[i].y;
	}
	cout << "done!" << endl;

	Eigen::MatrixXd matrixTestData(TEST_SIZE, SIZE_VECTOR_X);

	cout << "building test matrix X ...";
	for (int i = 0; i < TEST_SIZE; ++i) {
		for (int j = 0; j < SIZE_VECTOR_X; ++j) {
			matrixTestData(i, j) = myTestData[i].x[j];
		}
	}
	cout << "done!" << endl;








	//////////////////// SOLUTION 1: LINEAR REGRESSION ////////////////////

	
	/*
	Eigen::VectorXd betas(TRAINING_SIZE); // why are you using TRAINING_SIZE? shouldn't it be SIZE_VECTOR_X (15)
	// coefficient using QR decomposition
	cout << "computing betas from training set ...";

	// X^TX b = X^Ty
	betas = (matrixTrainingData.transpose()*matrixTrainingData).colPivHouseholderQr().solve(matrixTrainingData.transpose() * resultValues);
	cout << "done!" << endl;

	Eigen::VectorXd testResults(TEST_SIZE);
	// coefficient using QR decomposition
	cout << "computing results y from test set ...";
	// 
	testResults = matrixTestData * betas;
	cout << "done!" << endl;
	*/




	//////////////////// SOLUTION 2: POLYNOMIALS DEG 2 REGRESSION ////////////////////

	//just a test, don't uncomment this part
	/*
	MatrixXd oneRow = matrixTrainingData.row(0);
	cout << "ONEROW size" << oneRow.rows() << "," << oneRow.cols() << endl << endl;
	for (int i = 0; i < 15; i++) {
		cout << i << "\t" << oneRow(i) << endl;
	}

	MatrixOp myMOP;
	MatrixXd oneRowPoly2(1, 136);
	cout << "ONEROWPOLY2 size" << oneRowPoly2.rows() << "," << oneRowPoly2.cols() << endl << endl;
	myMOP.MakePoly2MatrixFromX(&oneRow, &oneRowPoly2);
	for (int i = 0; i < 136; i++) {
		cout << i << "\t" << oneRowPoly2(i) << endl;
	}
	*/

	

	/*
	cout << "Starting poly2 computation" << endl;
	MatrixXd matrixTrainingDataPoly2(TRAINING_SIZE, SIZE_VECTOR_X_POLY2);
	MatrixXd matrixTestDataPoly2(TEST_SIZE, SIZE_VECTOR_X_POLY2);

	MatrixOp myMOP;


	cout << "Starting matrixTrainingDataPoly2 computation ...";
	myMOP.MakePoly2MatrixFromX(&matrixTrainingData, &matrixTrainingDataPoly2);
	cout << "done!" << endl;
	cout << "Starting matrixTestDataPoly2 computation ...";
	myMOP.MakePoly2MatrixFromX(&matrixTestData, &matrixTestDataPoly2);
	cout << "done!" << endl;

	//same code as before

	VectorXd betas(SIZE_VECTOR_X_POLY2); // ???
	// coefficient using QR decomposition
	cout << "computing betas from training set ...";

	// X^TX b = X^Ty
	betas = (matrixTrainingDataPoly2.transpose()*matrixTrainingDataPoly2).colPivHouseholderQr().solve(matrixTrainingDataPoly2.transpose() * resultValues);
	cout << "done!" << endl;

	Eigen::VectorXd testResults(TEST_SIZE);
	// coefficient using QR decomposition
	cout << "computing results y from test set ...";
	testResults = matrixTestDataPoly2 * betas;
	cout << "done!" << endl;
	*/
	





	//////////////////// SOLUTION 3: POLYNOMIALS DEG N REGRESSION ////////////////////
	/*
	int deg;
	cout << "please insert degree for this regression (don't try more than 4 honestly)" << endl;
	cin >> deg;

	MatrixOp myMOP;

	int SIZE_D = myMOP.ComputePolyDimension(15, deg); // TODO
	cout << "using D=" << SIZE_D << endl;

	cout << "Starting polyN allocation ...";
	MatrixXd Xtrain_polyN(TRAINING_SIZE, SIZE_D);
	MatrixXd Xtest_polyN(TEST_SIZE, SIZE_D);
	cout << "done!" << endl;

	cout << "Starting Xtrain_polyN computation ...";
	myMOP.MakePolyN(&matrixTrainingData, &Xtrain_polyN, deg);
	cout << "done!" << endl;
	cout << "Starting Xtest_polyN computation ...";
	myMOP.MakePolyN(&matrixTestData, &Xtest_polyN, deg);
	cout << "done!" << endl;

	//same code as before
	VectorXd weights(SIZE_D); // old betas
	
	// coefficient using QR decomposition
	cout << "computing weights from training set ...";z7615
	// X^TX b = X^Ty
	// IS THERE A BETTER METHOD?
	
	// A.	jacobiSvd(ComputeThinU | ComputeThinV)		.solve(b)).format(HeavyFmt)
	// A.	colPivHouseholderQr()			.solve(b)).format(HeavyFmt)
	// (A.transpose() * A).		ldlt()		.solve(A.transpose() * b)).format(HeavyFmt)

	//most expensive line
	weights = (Xtrain_polyN.transpose()*Xtrain_polyN).jacobiSvd(ComputeThinU | ComputeThinV).solve(Xtrain_polyN.transpose() * resultValues);
	cout << "done!" << "\a" << endl; // plays sound

	VectorXd testResults(TEST_SIZE);
	// coefficient using QR decomposition
	cout << "computing results y from test set ...";
	testResults = Xtest_polyN * weights;
	cout << "done!" << endl;
	*/

	//////////////////// SOLUTION 4: POLYNOMIALS DEG N REGRESSION WITH PCA////////////////////
	// POLYNOMIALS DEG N 
<<<<<<< HEAD
	/*
=======

>>>>>>> 49f410d96fc6810421065817e0ba1dc52b191557
	int deg;
	cout << "please insert degree for this regression (don't try more than 4 honestly)" << endl;
	cin >> deg;

	MatrixOp myMOP;

	int SIZE_D = myMOP.ComputePolyDimension(15, deg); // TODO
	cout << "using D=" << SIZE_D << endl;

	cout << "Starting polyN allocation ...";
	MatrixXd Xtrain_polyN(TRAINING_SIZE, SIZE_D);
	MatrixXd Xtest_polyN(TEST_SIZE, SIZE_D);
	cout << "done!" << endl;

	cout << "Starting Xtrain_polyN computation ...";
	myMOP.MakePolyN(&matrixTrainingData, &Xtrain_polyN, deg);
	cout << "done!" << endl;
	cout << "Starting Xtest_polyN computation ...";
	myMOP.MakePolyN(&matrixTestData, &Xtest_polyN, deg);
	cout << "done!" << endl;
<<<<<<< HEAD

	// PCA 
	// linear regression with PCA ==> method 1 (comment part before and // method 2 line and uncomment // method 1 line)
	// non linear with degree n ==> method 2 (uncomment part before and and // method 2 line and comment // method 1 line)

	// 1. Compute empirical mean and subtract if from the training data

	cout << "empirical mean ...";
	//VectorXd empiricalMean(SIZE_VECTOR_X); // method 1
	//empiricalMean = matrixTrainingData.colwise().sum() / TRAINING_SIZE; // method 1 
	VectorXd empiricalMean(SIZE_D);
	empiricalMean = Xtrain_polyN.colwise().sum() / TRAINING_SIZE;
	double empiricalMeanResults = resultValues.sum() / TRAINING_SIZE;

	cout << "done" << endl << "empirical subtraction ...";
	VectorXd h = VectorXd::Ones(TRAINING_SIZE); 
	MatrixXd matrixTrainingDataMeanSub = Xtrain_polyN - h * empiricalMean.transpose();
	resultValues -= h * empiricalMeanResults;
	cout << "done!" << endl;

	// 2. Calculate the covariance matrix
	cout << "covariance ...";
	MatrixXd covariance = matrixTrainingDataMeanSub.transpose() * matrixTrainingDataMeanSub;
	cout << "done!" << endl;

	// 3. Find the eigenvectors and the eigenvalues of the covariance matrix
	cout << "eigen decomposition ...";
	EigenSolver<MatrixXd> es(covariance);
	VectorXd eigenValues = es.eigenvalues().real();
	MatrixXd eigenVectors = es.eigenvectors().real(); 
	cout << "done!" << endl;

	// 4. Sort eigenvalues in descending order and eigenvector in descending eigenvalues order
	cout << "sorting eigen decomposition ...";
	
	vector<eigenStruct> sortedEigenDec;
	for (int i = 0; i < eigenValues.size(); ++i) {
		sortedEigenDec.push_back(eigenStruct(eigenValues(i), eigenVectors.col(i)));
	}
	sort(sortedEigenDec.begin(), sortedEigenDec.end(), compare);
	
	//MatrixXd components(SIZE_VECTOR_X, SIZE_VECTOR_X); // method 1 
	MatrixXd components(SIZE_D, SIZE_D); // method 2
	
	for (int i = 0; i < sortedEigenDec.size(); ++i) {
		//cout << sortedEigenDec[i].lambda << endl;
		components.col(i) = sortedEigenDec[i].vector;
	}
	cout << "done!" << endl;

	// 5. Find the apprapriate number of rows to drop
	// cumulative energy over all eigenvectors
	double enAll = eigenValues.sum();

	// find j such that the cumulative energy up to the j-th vector divide by
	// the cumulative energy of all eigenvectors is bigger than a threshold
	
	double t = 0.9;
	int j = 0; // number of rows to retain

	for (int i = 0; i < sortedEigenDec.size(); ++i) {
		double enJth = 0;
		for (int k = 0; k < i; ++k) {
			enJth += sortedEigenDec[k].lambda;
		}
		//cout << "frac : " << enJth / enAll << "at j=" << i << endl;
		if (enJth / enAll >= t ) {
			j = i; 
			break;
		}
	}
	cout << " number of rows to retain: " << j << endl;

	// Rearrange the components matrix

	//MatrixXd matrixTrainingDataPCA = matrixTrainingDataMeanSub * components.block(0,0,SIZE_VECTOR_X,j); // method 1
	MatrixXd matrixTrainingDataPCA = matrixTrainingDataMeanSub * components.block(0, 0, SIZE_D, j); // method 2

	VectorXd be = (matrixTrainingDataPCA.transpose()*matrixTrainingDataPCA).colPivHouseholderQr().solve(matrixTrainingDataPCA.transpose() * resultValues);
	//VectorXd be2 = components.block(0, 0, SIZE_VECTOR_X, j) * be; // method 1
	VectorXd be2 = components.block(0, 0, SIZE_D, j) * be; // method 2

	VectorXd testResults(TEST_SIZE);
	// coefficient using QR decomposition
	cout << "computing results y from test set ...";
	//testResults = matrixTestData * be2; // method 1
	testResults = Xtest_polyN * be2; // method 2
	cout << "done!" << endl;
	*/

	
	//////////////////////// SOLUTION 5: SVM with Train/Validation ////////////////////
		/* Next steps / improvement
		1. Clean up the code
		2. Relace simple train/validation with cross-validation / k-fold
		3. Try different params.kernel_type
		4. Try CvSVM::NU_SVR regression
		5. Improve how parameters are changed after each test
		*/


	const int VALIDATION_SIZE = 100;
	const int NUMBER_OF_TESTS = 50;
	const int NEW_TRAINING_SIZE = TRAINING_SIZE - VALIDATION_SIZE;
	float Errors[NUMBER_OF_TESTS];

	float lowest_error = 99999999;

	int bestParam_svmType = 1;
	int bestParam_kernelType = 1;
	float bestParam_C = 1;
	float bestParam_p = 0.1;
	float bestParam_degree = 1.0;
	float bestParam_gamma = 0.1;
	float bestParam_coef0 = 0.1;
	float bestParam_nu = 0.1;

	int currentParam_svmType = 0;
	int currentParam_kernelType = 1;
	float currentParam_C = 1;
	float currentParam_p = 0.1;
	float currentParam_degree = 3.5;
	float currentParam_gamma = 0.1;
	float currentParam_coef0 = 0.1;
	float currentParam_nu = 0.1;

	const float paramMax_C = 50;
	const float paramMax_p = 1.0;
	const float paramMax_degree = 5.0;
	const float paramMax_gamma = 0.5;
	const float paramMax_coef0 = 1.0;
	const float paramMax_nu = 0.9;

	const float paramMin_C = 1;
	const float paramMin_p = 0.1;
	const float paramMin_degree = 1;
	const float paramMin_gamma = 0.1;
	const float paramMin_coef0 = 0.1;
	const float paramMin_nu = 0.1;

	// Set test data
	float trainingData[NEW_TRAINING_SIZE][SIZE_VECTOR_X];
	float yValuesTraining[NEW_TRAINING_SIZE];

	for (int i = 0; i < NEW_TRAINING_SIZE; i++) {
		yValuesTraining[i] = myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			trainingData[i][j] = myTrainingData[i].x[j];
		}
	}

	Mat trainingDataMat(NEW_TRAINING_SIZE, SIZE_VECTOR_X, CV_32FC1, trainingData);
	Mat resultMat(NEW_TRAINING_SIZE, 1, CV_32FC1, yValuesTraining);

	// Set validation data
	float validationData[VALIDATION_SIZE][SIZE_VECTOR_X];
	float yValuesValidation[VALIDATION_SIZE];

	for (int i = NEW_TRAINING_SIZE; i < TRAINING_SIZE; i++) {
		yValuesValidation[i - NEW_TRAINING_SIZE] = myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			validationData[i - NEW_TRAINING_SIZE][j] = myTrainingData[i].x[j];
		}
	}

	Mat validationDataMat(VALIDATION_SIZE, SIZE_VECTOR_X, CV_32FC1, validationData);

	// ---------------------------------------------------------------
	// Finished initialization

	for (int n = 0; n < NUMBER_OF_TESTS; n++) {
		// Update params
		currentParam_C = paramMin_C + (n * (paramMax_C / NUMBER_OF_TESTS));
		currentParam_p = paramMin_p + (n * (paramMax_p / NUMBER_OF_TESTS));
		currentParam_nu = paramMin_nu + (n * (paramMax_nu / NUMBER_OF_TESTS));
		currentParam_gamma = paramMin_gamma + (n * (paramMax_gamma / NUMBER_OF_TESTS));

		if (n % 2 == 0) {
			currentParam_svmType = 0;
			currentParam_kernelType = 1;
		}
		else {
			currentParam_svmType = 1;
			currentParam_kernelType = 2;
		}

		// Get params & train
		CvSVMParams params = GetParams(currentParam_svmType, currentParam_kernelType, currentParam_degree, currentParam_gamma, currentParam_coef0, currentParam_C, currentParam_nu, currentParam_p);

		CvSVM SVM;
		SVM.train(trainingDataMat, resultMat, Mat(), Mat(), params);
=======

	// PCA 
	// linear regression with PCA ==> method 1 (comment part before and // method 2 line and uncomment // method 1 line)
	// non linear with degree n ==> method 2 (uncomment part before and and // method 2 line and comment // method 1 line)

	// 1. Compute empirical mean and subtract if from the training data

	cout << "empirical mean ...";
	//VectorXd empiricalMean(SIZE_VECTOR_X); // method 1
	//empiricalMean = matrixTrainingData.colwise().sum() / TRAINING_SIZE; // method 1 
	VectorXd empiricalMean(SIZE_D);
	empiricalMean = Xtrain_polyN.colwise().sum() / TRAINING_SIZE;
	double empiricalMeanResults = resultValues.sum() / TRAINING_SIZE;

	cout << "done" << endl << "empirical subtraction ...";
	VectorXd h = VectorXd::Ones(TRAINING_SIZE);
	MatrixXd matrixTrainingDataMeanSub = Xtrain_polyN - h * empiricalMean.transpose();
	resultValues -= h * empiricalMeanResults;
	cout << "done!" << endl;

	// 2. Calculate the covariance matrix
	cout << "covariance ...";
	MatrixXd covariance = matrixTrainingDataMeanSub.transpose() * matrixTrainingDataMeanSub;
	cout << "done!" << endl;

	// 3. Find the eigenvectors and the eigenvalues of the covariance matrix
	cout << "eigen decomposition ...";
	EigenSolver<MatrixXd> es(covariance);
	VectorXd eigenValues = es.eigenvalues().real();
	MatrixXd eigenVectors = es.eigenvectors().real();
	cout << "done!" << endl;

	// 4. Sort eigenvalues in descending order and eigenvector in descending eigenvalues order
	cout << "sorting eigen decomposition ...";

	vector<eigenStruct> sortedEigenDec;
	for (int i = 0; i < eigenValues.size(); ++i) {
		sortedEigenDec.push_back(eigenStruct(eigenValues(i), eigenVectors.col(i)));
	}
	sort(sortedEigenDec.begin(), sortedEigenDec.end(), compare);

	//MatrixXd components(SIZE_VECTOR_X, SIZE_VECTOR_X); // method 1 
	MatrixXd components(SIZE_D, SIZE_D); // method 2

	for (int i = 0; i < sortedEigenDec.size(); ++i) {
		//cout << sortedEigenDec[i].lambda << endl;
		components.col(i) = sortedEigenDec[i].vector;
	}
	cout << "done!" << endl;

	// 5. Find the apprapriate number of rows to drop
	// cumulative energy over all eigenvectors
	double enAll = eigenValues.sum();

	// find j such that the cumulative energy up to the j-th vector divide by
	// the cumulative energy of all eigenvectors is bigger than a threshold

	double t = 0.9;
	int j = 0; // number of rows to retain

	for (int i = 0; i < sortedEigenDec.size(); ++i) {
		double enJth = 0;
		for (int k = 0; k < i; ++k) {
			enJth += sortedEigenDec[k].lambda;
		}
		//cout << "frac : " << enJth / enAll << "at j=" << i << endl;
		if (enJth / enAll >= t) {
			j = i;
			break;
		}
	}
	cout << "number of rows to retain: " << j << endl;

	// Rearrange the components matrix

	//MatrixXd matrixTrainingDataPCA = matrixTrainingDataMeanSub * components.block(0,0,SIZE_VECTOR_X,j); // method 1
	MatrixXd matrixTrainingDataPCA = matrixTrainingDataMeanSub * components.block(0, 0, SIZE_D, j); // method 2

	VectorXd be = (matrixTrainingDataPCA.transpose()*matrixTrainingDataPCA).colPivHouseholderQr().solve(matrixTrainingDataPCA.transpose() * resultValues);
	//VectorXd be2 = components.block(0, 0, SIZE_VECTOR_X, j) * be; // method 1
	VectorXd be2 = components.block(0, 0, SIZE_D, j) * be; // method 2

	VectorXd testResults(TEST_SIZE);
	// coefficient using QR decomposition
	cout << "computing results y from test set ...";
	//testResults = matrixTestData * be2; // method 1
	testResults = Xtest_polyN * be2; // method 2
	cout << "done!" << endl;
>>>>>>> 49f410d96fc6810421065817e0ba1dc52b191557

		//SVM.train_auto(trainingDataMat, resultMat, Mat(), Mat(), params, 10, CvSVM::get_default_grid(CvSVM::C),
			//CvSVM::get_default_grid(CvSVM::GAMMA), CvSVM::get_default_grid(CvSVM::P), CvSVM::get_default_grid(CvSVM::NU), 
			//CvSVM::get_default_grid(CvSVM::COEF), CvSVM::get_default_grid(CvSVM::DEGREE), false);
		//cout << "train done." << endl;

		// Validate & calculate error
		float validationResults[VALIDATION_SIZE];

		for (int i = 0; i < VALIDATION_SIZE; i++) {
			cv::Mat sample = validationDataMat.row(i);
			float result = SVM.predict(sample, false);
			// cout << result << endl;
			validationResults[i] = result;
		}

		float error = CalculateError(validationResults, yValuesValidation, VALIDATION_SIZE);
		Errors[n] = error;

		if (error < lowest_error) {
			lowest_error = error;

			bestParam_C = currentParam_C;
			bestParam_p = currentParam_p;
			bestParam_degree = currentParam_degree;
			bestParam_gamma = currentParam_gamma;
			bestParam_coef0 = currentParam_coef0;
			bestParam_nu = currentParam_nu;
			bestParam_svmType = currentParam_svmType;
			bestParam_kernelType = currentParam_kernelType;

			cout << "C: " << bestParam_C << endl << "p: " << bestParam_p << endl << "degree: " << bestParam_degree << endl;
			cout << "gamma: " << bestParam_gamma << endl;
			cout << "coef0: " << bestParam_coef0 << endl << "nu: " << bestParam_nu << endl << "svmtype: " << bestParam_svmType;
			cout << endl << "kernel: " << bestParam_kernelType << endl << endl;
		}

		// cout << "Error #" << n << " = " << error << endl;
	}

	cout << endl << "===============================" << endl;
	cout << "Lowest error: " << lowest_error << endl;


	// ==================================================================================
	//  Make final result
	// Set test data
	float trainingDataFinal[TRAINING_SIZE][SIZE_VECTOR_X];
	float yValuesTrainingFinal[TRAINING_SIZE];

	for (int i = 0; i < TRAINING_SIZE; i++) {
		yValuesTrainingFinal[i] = myTrainingData[i].y;
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			trainingDataFinal[i][j] = myTrainingData[i].x[j];
		}
	}

	Mat trainingDataMatFinal(TRAINING_SIZE, SIZE_VECTOR_X, CV_32FC1, trainingDataFinal);
	Mat resultMatFinal(TRAINING_SIZE, 1, CV_32FC1, yValuesTrainingFinal);


	CvSVMParams params = GetParams(bestParam_svmType, bestParam_kernelType, bestParam_degree, bestParam_gamma, bestParam_coef0, bestParam_C, bestParam_nu, bestParam_p);

	CvSVM SVM;
	SVM.train(trainingDataMatFinal, resultMatFinal, Mat(), Mat(), params);

	//cout << "Error #" << n << " = " << error << endl;

	float testData[TEST_SIZE][SIZE_VECTOR_X];

	for (int i = 0; i < TEST_SIZE; i++) {
		for (int j = 0; j < SIZE_VECTOR_X; j++) {
			testData[i][j] = myTestData[i].x[j];
		}
	}

	Mat testDataMat(TEST_SIZE, SIZE_VECTOR_X, CV_32FC1, testData);
	VectorXd testResults(TEST_SIZE);

	for (int i = 0; i < testDataMat.rows; i++) {
		cv::Mat sample = testDataMat.row(i);
		float result = SVM.predict(sample, false);
		testResults(i) = result;
	}
	//----------------------------------------------



	// write back
	
	string filename;
	cout << "Specify filename:" << endl;
	cin >> filename;

	cout << "wrinting results y to file results.csv ...";
	string pathName = "../../results/" + filename + ".csv";
	myreader.writeTestResult(pathName, testResults);
	cout << "done!" << endl;
	




	system("pause");
}
