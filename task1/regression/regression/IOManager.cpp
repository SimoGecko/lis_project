#include "IOManager.h"
#include "Util.h"



IOManager::IOManager()
{
}

IOManager::~IOManager()
{
}



void IOManager::readData(string path, DataEntry* myData, bool isTrainData) {

	//DataEntry myData[TEST_SIZE];
	ifstream trainData(path);
	if (trainData.is_open()) {

		string line;
		int lineCount = 0;

		//read line
		while (getline(trainData, line)) {

			istringstream ss(line);
			string token;
			DataEntry newData;

			int e = 0;

			// parse each line
			while (getline(ss, token, ',')) {
				if (isTrainData) {
					if (e == 0)			newData.Id		= atoi(token.c_str());
					else if (e == 1)	newData.y		= atof(token.c_str());
					else				newData.x[e-2]	= atof(token.c_str());

				}
				else {
					newData.y = -1;
					if (e == 0)			newData.Id		= atoi(token.c_str());
					else				newData.x[e-1]	= atof(token.c_str()); //-1 not -2
				}
				
				//cout << e << "__\t" << token << endl;
				e++;
			}
			myData[lineCount++] = newData;
			//myData[e]->print();
		}
	}
	trainData.close();
}


void IOManager::writeTestResult(string path, Eigen::VectorXd valuesY) {
	ofstream resultValues;
	resultValues.open(path); //should it be csv?

	if (resultValues.is_open()) {
		resultValues << "Id,y\n";


		for (int i = 0; i < valuesY.size(); ++i) {
			resultValues << to_string(i + 900) << "," << Util::to_string_with_precision(valuesY(i)) + "\n";//to_string(valuesY(i)) + "\n";
		}
	}
	resultValues.close();

}
