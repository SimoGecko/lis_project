#include "Util.h"


Util::Util()
{
}

Util::~Util()
{
}

string Util::to_string_with_precision(double a_value, const int n)
{
	ostringstream out;
	out << setprecision(n) << a_value;
	return out.str();
}

int Util::binom(int n, int k)
{
	int n_facdiv = 1;
	int k_fac = 1;

	for (int i = n; i > n - k; i--)
		n_facdiv *= i;
	for (int i = k; i > 1; i--)
		k_fac *= i;

	return n_facdiv / k_fac;
}