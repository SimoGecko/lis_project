#include "MatrixOp.h"
#include "Util.h"




MatrixOp::MatrixOp(){}

MatrixOp::~MatrixOp(){}


// here we take X (x1 to x15) and compute all coefficients for polynomial
// of degree 2, namely (1, x1-x15, xi*xj (i!=j), xi^2
// we have dim 15, the final matrix will have dim binom(15+2,2)=136
void MatrixOp::MakePoly2MatrixFromX(MatrixXd* X, MatrixXd* Xpoly2)
{
	int n = X->rows();
	Xpoly2->resize(n, 136);
	//MatrixXd Xpoly2(n, 136);
	VectorXd currX(15); // the current row in the matrix (a datapoint)
	VectorXd newX(136);

	for (int i = 0; i < n; i++) {
		//foreach entry
		for (int l = 0; l < 15; l++) {
			currX(l) = (*X)(i,l);
		}
		//currX = X->row(i).transpose;

		int o = 0; // offset

		// 1
		newX(o++) = 1;

		// x1 - x15
		for (int j = 0; j < 15; j++) {
			newX(o++) = currX(j);
		}

		// xi*xj forall i!=j
		for (int j = 0; j < 15; j++) {
			for (int k = j+1; k < 15; k++) {
				newX(o++) = currX(j)*currX(k);
			}
		}

		// xi^2
		for (int j = 0; j < 15; j++) {
			newX(o++) = currX(j)*currX(j);
		}

		//assign back
		for (int l = 0; l < 136; l++) {
			(*Xpoly2)(i,l) = newX(l);
		}
		//Xpoly2->row(i) = newX.transpose();
		
	}
}


// more general function that computes whichever degree you wish
void MatrixOp::MakePolyN(MatrixXd * X, MatrixXd * Xpoly, int deg)
{
	if (deg > 4 || deg < 1) deg = 1;

	int X_SIZE = X->cols()+1;
	int N_SIZE = X->rows();

	int D_SIZE = Xpoly->cols();//ComputePolyDimension(X_SIZE - 1, deg);//binom(X_SIZE + deg -1, deg);//(deg + 1);

	Xpoly->resize(N_SIZE, D_SIZE);

	VectorXd currX(X_SIZE); // the current row in the matrix (a datapoint)
	VectorXd newX(D_SIZE);

	//foreach entry
	for (int n = 0; n < N_SIZE; n++) {

		//copy by value
		currX(0) = 1;
		for (int l = 1; l < X_SIZE; l++) {
			currX(l) = (*X)(n, l-1);
		}
		//currX = X->row(i).transpose; // more efficient?

		int o = 0; // offset

		
		//TODO IMPLEMENT HIGHER DIMENSIONS!! NOW IT ASSUMES 3

		//new code, does all together
		if (deg == 2) {
			for (int i1 = 0; i1 < X_SIZE; i1++) {
				for (int i2 = i1; i2 < X_SIZE; i2++) {
					newX(o++) = currX(i1)*currX(i2);
				}
			}
		}
		else if (deg == 3) {
			for (int i1 = 0; i1 < X_SIZE; i1++) {
				for (int i2 = i1; i2 < X_SIZE; i2++) {
					for (int i3 = i2; i3 < X_SIZE; i3++) {
						newX(o++) = currX(i1)*currX(i2)*currX(i3);
					}
				}
			}
		}
		else if (deg == 4) {
			for (int i1 = 0; i1 < X_SIZE; i1++) {
				for (int i2 = i1; i2 < X_SIZE; i2++) {
					for (int i3 = i2; i3 < X_SIZE; i3++) {
						for (int i4 = i3; i4 < X_SIZE; i4++) {
							newX(o++) = currX(i1)*currX(i2)*currX(i3)*currX(i4);
						}
					}
				}
			}
		}
		else { // default deg == 1
			for (int i1 = 0; i1 < X_SIZE; i1++) {
				newX(o++) = currX(i1);
			}
		}
		

		//assign back
		for (int l = 0; l < D_SIZE; l++) {
			(*Xpoly)(n, l) = newX(l);
		}
		//Xpoly2->row(i) = newX.transpose();
	}
}

int MatrixOp::ComputePolyDimension(int x_size, int deg) {
	return Util::binom((x_size + 1) + deg - 1, deg); // assumes x doesn't contain the leading 1 "dimension"
}


