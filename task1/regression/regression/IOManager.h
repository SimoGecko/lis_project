// this class deals with reading from file and writing to file

#include "includeList.h"


struct DataEntry
{
	unsigned int Id;
	double y;
	double x[15];
	DataEntry() {
		Id = -1;
		y = 0;
		for (int i = 0; i < 15; i++) x[i] = 0;
	}

	void print() {
		cout << "Id:\t" << Id << endl;
		cout << "y:\t" << y << endl;
		for (int i = 0; i < 15; i++) cout << "x" << i + 1 << ":\t" << x[i] << endl;
		cout << endl;
	}
};




class IOManager
{
public:
	IOManager();
	~IOManager();


	void readData(string path, DataEntry* myData, bool isTrainData);
	void writeTestResult(string path, Eigen::VectorXd valuesY);

private:

};