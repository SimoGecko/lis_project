#pragma once
#include "includeList.h"

/* Useful links
- Install & deploy OpenCV: http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_visual_studio_Opencv/windows_visual_studio_Opencv.html#windows-visual-studio-how-to
- SVM Introduction: http://docs.opencv.org/2.4/doc/tutorials/ml/introduction_to_svm/introduction_to_svm.html
- SVM Documentary: http://docs.opencv.org/2.4/modules/ml/doc/support_vector_machines.html
*/


// Helper functions
float CalculateError(float* predictedResults, float* actualResults, int n) {
	float RMSE = 0.0;
	float sum = 0.0;
	float sq = 1.0 / n;

	for (int i = 0; i < n; i++) {
		sum += (predictedResults[i] - actualResults[i]) * (predictedResults[i] - actualResults[i]);
	}

	RMSE = sqrt(sq * sum);

	return RMSE;
}


/*
svm_type = 0 -> EPS_SVR
svm_type = 1 -> NU_SVR

kernel_type = 0 -> LINEAR
kernel_type = 1 -> POLY
kernel_type = 2 -> RBF
kernel_type = 3 -> SIGMOID
*/
CvSVMParams GetParams(int svm_type, int kernel_type, double degree, double gamma, double coef0, double C, double nu, double p) {
	CvSVMParams params;

	// SVM_TYPE
	if (svm_type == 0)
		params.svm_type = CvSVM::EPS_SVR;
	else
		params.svm_type = CvSVM::NU_SVR;

	// KERNEL_TYPE
	if (kernel_type == 0)
		params.kernel_type = CvSVM::LINEAR;
	else if (kernel_type == 1)
		params.kernel_type = CvSVM::POLY;
	else if (kernel_type == 2)
		params.kernel_type = CvSVM::RBF;
	else
		params.kernel_type = CvSVM::SIGMOID;

	// REST
	params.C = C; // for CV_SVM_C_SVC, CV_SVM_EPS_SVR and CV_SVM_NU_SVR
	params.p = p; // for CV_SVM_EPS_SVR
	params.degree = degree; // for poly
	params.gamma = gamma; // for poly/rbf/sigmoid
	params.coef0 = coef0; // for poly/sigmoid
	params.nu = nu; // for CV_SVM_NU_SVC, CV_SVM_ONE_CLASS, and CV_SVM_NU_SVR

	params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, 10000, 1e-12);

	return params;
}