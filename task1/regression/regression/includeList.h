// this header is used to avoid including duplicates

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <Eigen\Dense>
#include <Eigen\Eigenvalues>
#include <vector>
#include <iomanip>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\ml\ml.hpp>

//#include "Util.h"

using namespace std;
using namespace Eigen;

